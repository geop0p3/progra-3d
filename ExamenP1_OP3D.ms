/*
Carlos Ignaco Flores Soto
12/04/2018
*/

--Abrir registro de actividades:
OpenLog "C:\Users\carlos\Documents\tridimin\FloresCarlos_OP3D_E.txt" mode:"a" outputonly:false

--Arreglos de Colores
colorRGB = #(RED, GREEN, BLUE)
colorCMY = #(color 0 255 255, color 255 0 255, color 255 255 0)

--Creacion de Objetos con su respectiva Posicion y Propiedades
for i=1 to 3 do (
	box name: ("B" + i as string) wirecolor: colorRGB[i] pos: [0, (0+(i-1)*45), 0]
	omnilight name: ("L" + i as string) pos: [30, (0+(i-1)*45), 0]
	point name: ("H" + i as string) wirecolor: colorRGB[i] pos: [-30, (0+(i-1)*45), 0]
	teapot name: ("T" + i as string) wirecolor: colorCMY[i] pos: [75, (0+(i-1)*45), 0] radius: 15
	circle name: ("C" + i as string) wirecolor: colorCMY[i] pos: [120, (0+(i-1)*45), 0] radius: 3
)

--Creacion de Arreglos
BA = for i in geometry where classof i == box collect i
TA = for i in geometry where classof i == teapot collect i
TCA = for i in objects where (classof i == teapot or classof i == circle) collect i
TCA2 = for i in objects where (classof i == teapot and classof i == circle) collect i
--Secuencia num 1,1,2,2,3,3
num = #()
for i = 1 to 2 do(
	for j = 1 to 3 do num[(j*2)-(i-1)] = j
)

--Creacion de Cajas, Teteras y la asignacion de sus Pivotes
for i = 1 to 3 do(
	t = teapot name: "T" pos:[BA[i].center.x, BA[i].center.y, BA[i].max.z] wirecolor: colorCMY[i] radius: 6
	b = box name: "C" wirecolor: colorRGB[i] pos:[TA[i].center.x, TA[i].center.y, TA[i].max.z] height: 6 width: 6 length: 6

	t.pivot = TA[i].pivot
	b.pivot = BA[i].pivot
)

--Recolectar Objetos de Color Rojo, Verde y Azul
HC = for i in geometry where i.wirecolor == RED or i.wirecolor == GREEN or i.wirecolor == BLUE collect i

--Creacion de Arreglo de Point Helpers
PA = for i in objects where classof i == point collect i

--Asignar Hijos a los Point Helpers
for i = 1 to HC.count do(
	if HC[i].wirecolor == RED then HC[i].parent = PA[1]
	else if HC[i].wirecolor == GREEN then HC[i].parent = PA[2]
	else if HC[i].wirecolor == BLUE then HC[i].parent = PA[3]
)

--Creacion de Arreglo de Teteras chicas
ST = for i in geometry where classof i == teapot and i.radius == 6 collect i

--Recolectar Objetos que no sean Teteras chicas de Color Cyan, Verde y Azul
TC = for i in objects where (i.wirecolor == colorCMY[1] or i.wirecolor == colorCMY[2] or i.wirecolor == colorCMY[3]) and i.radius != 6 collect i

--Asignar Hijos de Teteras chicas usando num
n = 0
for i = 1 to TC.count do
(
	 n = num[i]
	if TC[i].wirecolor == ST[n].wirecolor then TC[i].parent = ST[n];
)

--Creacion del Arreglo de Luces
LA = for i in objects where classof i == omnilight collect i

--Creacion del Arreglo de los Objetos que no sean Luces
NLA = for i in objects where classof i != omnilight collect i

--Seleccionar la Posicion aleatoria en X y Y de todos los Objetos en escena y asignarlo al valor Z de la Posicion de la Luz
for i = 1 to LA.count do(
	LA[i].pos.z = 0.8 * (NLA[random 1 NLA.count].pos.x + NLA[random 1 NLA.count].pos.y)
)

--Rotar los Objetos que tengan mas de 1 Hijo
PARENTS = for i in objects where i.children.count > 1 collect i
for i = 1 to PARENTS.count do rotate PARENTS[i] (eulerAngles 0 0 45)

--Imprimir la informacion de todos los objetos en forma de lista empezando por los de clase Point, Teapot y Circle
--Y al final los de clase Box y Omnilight

format "NOMBRE\t\tTIPO\t\tCOLOR\t\t#HIJOS\t\tPOSICION\t\t\t\tROTACION\n"
for i = 1 to objects.count do (
	if classof objects[i] == point or classof objects[i] == teapot or classof objects[i] == circle then(
	format "%\t\t%\t\t  %\t\t%\t\t%\t\t\t\t%\n" objects[i].name objects[i].name objects[i].wirecolor objects[i].children.count objects[i].pos objects[i].rotation)
)
for i = 1 to objects.count do (
	if classof objects[i] == box or classof objects[i] == omnilight then(
	format "%\t\t%\t\t  %\t\t%\t\t%\t\t\t\t%\n" objects[i].name objects[i].name objects[i].wirecolor objects[i].children.count objects[i].pos objects[i].rotation)
)