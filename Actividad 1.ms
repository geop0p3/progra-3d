-- Carlos Ignacio Flores Soto 8/ marzo / 2018
-- Actividad 1 Sintaxis y Declaraciones de variables
A=#(1, "string", color 255 255 34) -- arreglo de 3 espacios
A=#() -- arreglo vacio
A.count -- regresa tamaño
CLASSOF -- Regresa la clase de un objeto. Necesitas darle un valor
GC() -- Garbage Collector, Borra memoria. No ocupa valor
CLASSOF B== INTEGER -- Checa si la variable B es int
B<=1 -- Compara B si es menor o igual a 1
$TETERA -- Usas el objeto con nombre TETERA
Color 255 255 40 -- Regresa Color
select $TETERA -- Selecionas teerea
$Tetera: nombre --Cambias nombre de Tetera