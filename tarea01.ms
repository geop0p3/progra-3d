-- Tarea 1
-- Hacer una esfera con su pivote en su base que se llame "E1" y tenga el color azul
-- Hacer una tetera con el nombre "T1" con color verde
-- Crear dos point helpers
-- Posicionar a la esfera en [0, 40 ,0]
-- posicionar a la tetera en [40, 0, 0]
-- posicionar a cada uno de los Point helpers en la posicion del pivote de cada una de las figuras.

sphere name:"e1"
$.wirecolor = blue
$e1.recenter=true
$e1.pos=[0,40,0]
teapot name:"t1"
$.wirecolor = green
$t1.pos=[40,00,0]
point name:"p1"
$p1.pos=[$e1.min.z,$e1.center.x, $e1.max.z]
point name:"p2"
$p2.pos=[$t1.min.z,$t1.center.x, $t1.max.z]