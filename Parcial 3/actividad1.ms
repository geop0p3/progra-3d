/*Actividad 1
Junio 6
Carlos Ignacio Flores Soto

Dividir función en funciones pequeñas
Cambiar formats a messagebox
*/
openlog "C:\Users\carlos\Documents\tridimin\log220318.txt" mode: "a"
fn acomodo objetos = (
	Objs=#() --Arreglo para objetos diferentes a geometria
	ObjG=#()--Arreglo para objetos de tipo geometria
	struct InfoG (NCaras, Nombre, Ocolor) --Estructura para guardar los arreglos de cada parametro
	for j in objetos where (superclassof j == geometryclass) do(append ObjG j)
	if (ObjG.count >0 and objetos !=undefined) then
		(
			NC=#() --Numero de caras
			NO=#()--Nombre objetos
			CO=#()-- Color objetos
			for i=1 to selection.count where (superclassof selection[i] == geometryclass) do (append NC ((getPolygonCount selection[i])[1]); append NO (Selection[i].name); append CO (Selection[i].wirecolor))
			Obje= infoG Ncaras:NC Nombre:NO OColor:CO
	    HP= findItem Obje.NCaras	(amax Obje.NCaras) --funcion que busca el valor y regresa el indice donde se encontro (funcion que localiza el valor maximo en el arreglo)
		    HP= findItem Obje.NCaras	(amax Obje.NCaras) --funcion que busca el valor y regresa el indice donde se encontro (funcion que localiza el valor maximo en el arreglo)
			InstanceHP= (execute ("instance "+"$"+Obje.Nombre[HP]))
			InstanceHP.pos=[0,0,0]; InstanceHP.name=InstanceHP.name+"_HP"
			LP=findItem Obje.NCaras (amin Obje.NCaras) --funcion que localiza el valor minimo en el arreglo
			InstanceLP= (execute ("instance "+"$"+Obje.Nombre[LP]))
			InstanceLP.pos=[0,0,0]; InstanceLP.name=InstanceLP.name+"_LP"
		)
)

fn instancias objetos = (
	Objs=#() --Arreglo para objetos diferentes a geometria
	ObjG=#()--Arreglo para objetos de tipo geometria
	InstanceHP= (execute ("instance "+"$"+Obje.Nombre[HP]))
	InstanceHP.pos=[0,0,0]; InstanceHP.name=InstanceHP.name+"_HP"
	LP=findItem Obje.NCaras (amin Obje.NCaras) --funcion que localiza el valor minimo en el arreglo
	InstanceLP= (execute ("instance "+"$"+Obje.Nombre[LP]))
	InstanceLP.pos=[0,0,0]; InstanceLP.name=InstanceLP.name+"_LP"
)

fn imprimir objetos = (
	Objs=#() --Arreglo para objetos diferentes a geometria
	ObjG=#()--Arreglo para objetos de tipo geometria
	for i in objetos where (superclassof i != geometryclass) do (append Objs ((superclassof i)as String))
	format "Tipo de objetos: "
	for j=1 to Objs.count do (format "%, " (Objs[j])); format "Cantidad de objetos que no son geometria(s): % "(Objs.count as String)
	Cat=#(HP,LP)
	CatIns=#(InstanceHp.Name, InstanceLP.name)
	format "#Obj Clasif\t\tNombre Original\t\tWirecolor Original\t\tConteo de Caras\t\tNombre Actual\t\t\t     "
	for i=1 to Cat.count do(
	format "%\t\t\t\t%\t\t\t\t%\t\t\t%\t\t\t\t%\n" Cat[i] Obje.Nombre[Cat[i]] Obje.Ocolor[Cat[i]] Obje.NCaras[Cat[i]] CatIns[i])
)
closelog()