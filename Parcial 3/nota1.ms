function apilar a b = (
	a.pos=[b.center.x, b.center.y, b.max.z]
)

-- Creación de objetos
box name: "b1"
sphere name: "s1"
apilar (cylinder()) $b1
apilar2 (box()) (sphere())

-- Condiciones para no manejar variables incorrectas
-- Se puede declarar la funcion con fn o function
fn apilar2 a b = (
	if a!=undefined and b!=undefined and superclassof a == geometryclass then(
		a.pos =  [b.center.x, b.center.y, b.max.z]
	)
)

apilar2 j y
fn apilarObjs=
(
	b=2
	for i=1 to (selection.count-1)do(
		selection[b].pos=[selection[i].center.x, selection[i].center.y, selection[i].max.z]
		b=b+1
	)
)

apilarObjs
fn Parent objetos = (
	for i=1 to (selection.count - 1) do
	(
		$[i].parent=$[i+1]
	)
)


-- Para animar con keyframes
Parent
fn RotObj objeto velocidad = (
	with animate on
		for i=0 to 100 do(
			at time i
				objeto.rotation.z_rotation = i * velocidad
		)
)
