-- 7 de Junio
-- Actividad de funciones

-- Iniciso A - La función se llama cm
-- debe imprimir con format o print el color y el nombre
-- de cualquier objeto que le envien
openlog "C:\Users\carlos\Documents\tridimin\log220318.txt" mode: "a"

fn cm objeto = (
  obj=#()
  for i in objeto do (append obj i)
  if (obj.count > 0 and objeto !=undefined) then (
    for j to obj.count do (
      format "Nombre:% Color:% " obj[j].name obj[j].wirecolor
    )
  )
)

/*
Inciso B
Hacer una funcion que se llame crear que recibe como parametro el nombre, color
y un numero.
El nombre y color se le asignara al constructor de una caja, el numero se usara
para las propiedades de dimensiones. El numero se multiplica por 10 y se usa para su posicion.
*/

fn crear nombre numero colors =(
  if (classof nombre == string and classof colors == color and classof numero == integer or classof numero == float) do (
    pos = numero * 10
    box name:(nombre) wirecolor:(colors) width: (numero) height:(numero) length: (numero) pos:[pos,pos,pos]
  )
)

/*
Actividad 3
Hacer una funciona que reciba n cantidad de objetos
Asignarle primero los colores de rgb y cmy en orden
Si son mas de 6 debes asignarle colores random entre rojo y amarillo
*/

fn colores objetos = (
  colorines = #(RED, GREEN, BLUE, (color 0 255 255), (color 255 0 255), yellow)
  if (objetos.count > 0 and objetos !=undefined) do (
    for j=1 to 6 do (
      objetos[j].wirecolor= colorines[j]
    )
    if objetos.count > 6 do (
      for k = 7 to objetos.count do (
        objetos[k].wirecolor = (random red yellow)
      )
    )
  )
)
closelog()