/*
Carlos Ignacio Flores Soto
7 Junio 2018
*/

openlog "C:\Users\carlos\Documents\tridimin\ams.txt" mode: "a"

/*
LA FUNCION SE DEBE LLAMAR CM
DEBE IMPRIMIR CON FORMAT O PRINT EL COLOR Y EL NOMBRE
DEL OBJETO QUE LE PASEN
*/

box name: "b1"
sphere name: "s1"

fn CM obj =
(
	format "Nombre: %   Color: % " obj.name obj.wirecolor
)

CM $b1
/*
ACTIVIDAD 2
HACER UNA FUNCION QUE SE LLAME CREAR
QUE RECIBA COMO PARAMETROS EL NOMBRE, COLOR Y UN NUMERO
EL NOMBRE Y COLOR SE LE ASIGNARA AL CONSTRUCTOR DE UNA
CAJA, EL NUMERO SE USARA PARA LAS PROPIEDADES DE DIMENSIONES
Y DE POSICION DE ESTE ULTIMO EL NUMERO SE MULTIPLICARA POR 10
*/

fn CREAR nombre col num=(
	if classOf nombre != string then(
		nombre = "nada"
	)

	if classOf col != color then(
		col = black
	)

	if SuperClassOf num != number then(
		num = -1
	)

	b = box name: nombre wirecolor: col width: num height: num length: num

	b.pos *= num
)


nombre = "caja"
col = RED
num = 10

CREAR nombre col num

/*
ACTIVIDAD 3
HACER UNA FUNCION QUE RECIBA N CANTIDAD DE OBJETOS
A ESTOS DEBE ASIGNARLES PRIMERO LOS COLORES DE RGB Y CMY
EN ORDEN, SI LOS OBJETOS RESULTAN SER MAS DE 6 DEBE ASIGNARLES
UN COLOR RANDOM ENTRE ROJO Y AMARILLO
*/



fn rand obj = (
	colors = #(RED, GREEN, BLUE, color 0 255 255, color 255 0 255, YELLOW)
	randColors = #(RED, YELLOW)

	for i = 1 to obj.count do(
		if i > 6 then(
			obj[i].wirecolor = randColors[(random 1 2)]
		)
		else(
			obj[i].wirecolor = colors[i]
		)
	)
)

obj = for i in objects collect i

rand obj

closelog
