/*
Interface Configuracion de Escena
Carlos Ignacio Flores Soto OP3D E3
18 de Junio del 2018
*/
openlog "F:\Escuela\Tridimin\florescarlos_OP3D_E_3.txt" mode: "a"

fn quitaespacios str = (
	tmp = findstring str " "
	global noespacios = replace str tmp 1 "_"
)

rollout ConfigurarEscena "Configurar Escena" width:246 height:312
(
	GroupBox 'grp1' "Configurar Escena" pos:[3,8] width:237 height:298 enabled:true align:#left
	label 'lbl1' "Nombre De Artista" pos:[8,40] width:99 height:17 align:#left
	edittext 'edt1' "artistastr" pos:[99,40] width:130 height:17 align:#left
	label 'lbl2' "Titulo de Trabajo" pos:[10,72] width:91 height:15 align:#left
	edittext 'edt2' "trabajostr" pos:[99,71] width:129 height:17 align:#left
	button 'btn1' "Actualizar" pos:[13,108] width:92 height:22 align:#left
	button 'btn2' "Configurar Escena" pos:[117,108] width:110 height:22 align:#left
	label 'lbl3' "Imagen de Referencia" pos:[11,154] width:109 height:18 align:#left
	pickbutton 'btn3' "Elegir Imagen" pos:[127,153] width:107 height:20 align:#left
	bitmap 'bmp1' "Bitmap" pos:[22,185] width:203 height:67 align:#left
	button 'btn4' "Crear" pos:[47,268] width:147 height:24 align:#left
	on edt1 entered artistastr do (
		if text != "" do (
			global artista = artistastr
				)
			)
		)
	on edt2 entered trabajostr do (
		if text != "" do (
			global trabajo = trabajostr
				)
			)
		)
	on btn1 pressed do (
		quitaespacios artista
		artista = noespacios
		quitaespacios trabajo
		artista = noespacios
	)
	on btn2 pressed do (
		units.Display = #Metric
		units.MetricType = #Centimeters
	)
	on btn3 picked do (
		global imagen = getOpenFileName caption:"Elige la imagen" \
		type: "JPEG(*.jpeg)|*.jpeg|PNG(*.png)|*.png|JPG(*.jpg)|*.jpg|"
		global anchura = imagen.width
 		global altura = imagen.height
	)
	bitmap bmp1 bitmap:imagen

	on btn4 pressed do (
		plane name:"plano" width: anchura*0.01 height: altura*0.01
		plano.showFrozenInGray = false
		plano.Freeze = true
		backgroundColor = [9, 9, 9]
		plano.meditmaterials[1].diffusemap.filename = imagen
	)
createDialog ConfigurarEscena

closelog(
