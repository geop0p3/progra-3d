openlog "C:\Users\pope_\Documents\Tridimin\FLORESCARLOS_OP3D_A.txt" mode: "a" --Abre un log y lo crea

-- Carlos Ignacio Flores Soto
-- Actividades 1 a 4 / Abril 19 del 2018
--
-- ACTIVIDAD 1
-- CREAR UNA CAJA QUE SE LLAME CAJA1 Y TENGA DE DIMENSIONES
-- IGUALES A 25 UNIDADES CON EL COLOR ROJO, ESTO USANDO EL
-- CONSTRUCTOR
-- box() --crea un caja

Caja=box name:"CAJA1" width:25 length:25 wirecolor:RED

-- ACTIVIDAD 2
-- CREAR POR MEDIO DE UN CONSTRUCTOR UNA ESFERA AMARILLA CON UN RADIO DE
-- 10 UNIDADES EN LA POSICIÓN [0,0,25] CON EL NOMBRE E1
-- sphere() -- crea una esfera

Esfera= sphere name:"E1" radius:10 pos:[0,0,25] wirecolor:[255,255,0]

-- ACTIVIDAD 3
-- CAMBIAR EL NOMBRE DE LA CAJA1 A C1
-- nombre.name= "nombre" --cambia el nombre del obejto
Caja.name= "C1"

-- ACTIVIDAD 4
-- CAMBIAR EL COLOR DE LA ESFERA A VERDE Y EL DE LA CAJA AZUL
-- nombre.wirecolor = [color] --cambia el color del wireframe
Caja.wirecolor= GREEN
Esfera.wirecolor= BLUE
closelog() --cierra archivo