-- actividad por medio de un for crear 4 cajas separadas 30 unidades de cada una en el eje x el color de cada una debe corresponder a
-- rojo, erde, azul, magenta en el codigo deben hacer uso de un for, la declaracion del constructor y declaracion de los colores desde un arreglo , el cual iteraran en el for
-- para acceder a estos valores

a = #()
c = #(color 255 0 0, color 0 255 0, color 0 0 255, color 255 0 255)
x = 0; y = 0
for j = 1 to c.count do(
    a[i] = box ()
    a[i].wirecolor = c [i]
    a[i].pos = [x, y, 0]
    x += 30
)

-- actividad 2 por medio de un for anidado hace 4 hileras de cajas con 4 cajas por hilera la primera hilera estaran 4 cajas rojas, la segunda cuatro cajas verdes y
-- asi sucesivamente hasta la ultima hilera.

arreglo = #()
colores = #(color 255 0 0, color 0 255 0, color 0 0 255, color 255 0 255)
x = 0; y = 0
for i = 1 to colores.count do (
    for j = 1 to colores.count do(
        arreglo[i] = box()
        arreglo[i].wirecolor = c[i]
        arreglo[i].pos = [x, y, 0]
        x += 30
    )
    x = 0
    y += 30
)
