openlog "C:\Users\pope_\Documents\Tridimin\FLORESCARLOS_OP3D_E2.txt" mode: "a" --Abre un log y lo crea

-- Carlos Ignacio Flores Soto
-- Actividad Mayo 17 del 2018

-- A
-- Crear un arreglo llamado EditNomObj que contenga los objetos de la escena
-- Una variable de número que contenga un valor random de 1 a la cantidad de objetos en la escena.
-- Se debe crear un arreglo o usar alguna variable mapeada para filtrar a solo los objetos de tipo Helpers, y renombrar la cantidad de helpers según la variable de número,
-- Para esto debe existir el código que garantice que el valor de la variable número es menor o igual a la cantidad de helpers en la escena, en caso de que el valor de la variable número sea mayor que la cantidad de helpers, debe existir el código necesario para volver a asignarle un valor válido para la variable numero que vaya de 1 a la cantidad de helpers en la escena.
-- Por ejemplo si hay 5 helpers en escena y la variable numero tiene el valor de 8, el código debe generar un nuevo valor de 1 a 5 para entonces nombrar la cantidad de helpers resultante, digamos que el random de 1 a  5 da 3, entonces el código debe nombrar los primeros 3 objetos de tipo helper en la escena.
-- La forma del nombre debe iniciar con el prefijo H_ + el nombre base del objeto + numerador, por ejemplo “H_Point Helper_1”; el numerador estará dado por la cantidad de helpers a cambiar de nombre.


EditNomObj = #()
helpz = #()
geometriax = #()
sinnombre = #()
EditNomObj = for i in objects collect i
helpz = for i in helpers collect i
numero = random 1 EditNomObj.count

for i = 1 to numero do (
  if numero < helpz.count then (
    helpz[i].name = "H_" + helpz[i].baseobject as string + i as string
  ) else (
    append sinnombre helpz[i] -- ARREGLAR ESTO
    numero = random 1 helpz.count
  )
)


-- Utilizando la misma variable número debe generar otro valor utilizando un random de 1 a la cantidad de objetos de tipo geometría en base al arreglo EditNomObj.
-- Se debe crear un arreglo o usar alguna variable mapeada para filtrar a solo los objetos de tipo Geometría, y renombrar la cantidad de Geometrías según la variable de número,
-- Para esto debe existir el código que garantice que el valor de la variable número es menor o igual a la cantidad de geometrías en la escena, en caso de que el valor de la variable número sea mayor que la cantidad de geometrías, debe existir el código necesario para volver a asignarle un valor válido para la variable numero que vaya de 1 a la cantidad de geometrías en la escena.
-- Por ejemplo si hay 5 geometrías en escena y la variable numero tiene el valor de 8, el código debe generar un nuevo valor de 1 a 5 para entonces nombrar la cantidad de geometrías resultante, digamos que el random de 1 a  5 da 3, entonces el código debe nombrar los primeros 3 objetos de tipo geometrías en la escena.
-- La forma del nombre debe iniciar con el prefijo G_ + el nombre base del objeto + numerador, por ejemplo “G_Box_1”; el numerador estará dado por la cantidad de geometrías a cambiar de nombre.
-- Al terminar el proceso debe mostrar en un solo messagebox cuantos helpers y cuantas geometrías NO cambiaron de nombre.

r = 0
texto = ""
for i=1 to EditNomObj.count do (
  if SuperclassOf EditNomObj[i] == GeometryClass then (
    r += 1
  )
)
numero = random 1 r

geometriax = for i in geometry collect i
for i = 1 to numero do (
  if numero < geometriax.count then (
    geometriax[i].name = "G_" + geometriax[i].baseobject as string + i as string
  ) else (
    append sinnombre geometriax[i] -- ARREGLAR ESTO
    numero = random 1 geometriax.count
  )
)

for i=1 to sinnombre.count do (
  texto = texto + sinnombre[i] as string + "\n"
)

messagebox (texto)


-- C
-- Hacer un arreglo llamado ConfigScena que contenga todos los objetos en escena.
-- En base a este arreglo debe comprobar que es mayor a 0 objetos, si lo es debe cambiar la configuración de la escena el color del Background a el color 9,9,9;
-- las unidades de la escena deben cambiar a display en metros y en base a centímetros, a todos los objetos de tipo geometría en CofigScena se les debe activar
-- la opción de BackfaceCull y a todos los objetos de la escena se les debe desactivar la propiedad de Show Frozen in Gray.

ConfigScena = #()
ConfigScena = for i in objects collect i
if ConfigScena.count > 0 then (
  backgroundcolor = [9,9,9]
)

units.displaytype = #Metric
units.metrictype = #Centimeters

for i=1 to ConfigScena.count do (
  if SuperclassOf ConfigScena[i] == GeometryClass then (
    ConfigScena[i].backfacecull = true
  ) else (
    ConfigScena[i].showfrozeningray = false
  )
)

-- E
-- A Partir de una variable llamada NRandom debe generar un número random entre 20 y 80, con el resultado
-- Debe hacer el código necesario para crear las siguientes Teteras en base a la cantidad que resulte de NRandom
-- Si el resultado de NRandom está dentro del random de 20 a 40 debe crear en random Teteras Rojas y Azules con un radio de 10 unidades y
-- sus posiciones debe darse por la división de los valores numéricos  del color entre 10, por ejemplo para rojo el valor en rgb es 255 0 0 la posición de esta tetera debe ser [25.5,0,0]
-- Si el resultado de NRandom está dentro del random de 41 a 50 debe crear en random Teteras verdes y Amarillas un radio de 5 unidades, y seguir el mismo criterio para su posición como se indica arriba.
-- Si el resultado de NRandom está dentro del random de 51 a 70 debe crear en random Teteras con un color random entre Cian y Magenta con un radio igual a 2.5, igual seguir el criterio para su posición.
-- Si el resultado de NRandom está dentro del random de 71 a 80 debe crear en Teteras cada una de las Teteras creadas se les asignará un
-- Wirecolor diferente iniciando por rojo, verde, azul, amarillo si la cantidad de teteras excede 4 objetos, los demás se les asignará un color random entre rojo y amarillo

NRandom = random 20 80
if NRandom >= 20 and NRandom <= 40 then (
  num2 = random 1 5
  for i=1 to num2 do (
    teapot name: ("tr_" + i as string) wirecolor: RED radius: 10 pos: [mod 255 10, 0 ,0]
    teapot name: ("ta_" + i as string) wirecolor: BLUE radius: 10 pos: [0, 0 ,mod 255 10]
  )
)

if NRandom >= 41 and NRandom <= 50 then (
  num2 = random 1 5
  for i=1 to num2 do (
    teapot name: ("tv_" + i as string) wirecolor: GREEN radius: 5 pos: [0, mod 255 10 ,0]
    teapot name: ("tam_" + i as string) wirecolor: [255, 255, 0] radius: 5 pos: [mod 255 10, mod 255 10 ,0]
  )
)

if NRandom >= 51 and NRandom <= 70 then (
  num2 = random 1 5
  for i=1 to num2 do (
    teapot name: ("tcm_" + i as string) wirecolor: [255, random 0 255, random 0 255] radius: 2.5 pos: [mod 255 10, mod 255 10 ,mod 255 10]
  )
)

if NRandom >= 71 and NRandom <= 80 then (
  num2 = random 1 5
  if num2 <= 4 then (
    for i=1 to num2 do (
      teapot name: ("to_" + i as string) wirecolor: [random 0 255, random 0 255, random 0 255]
    )
  ) else (
    for i=1 to num2 do (
      teapot name: ("to_" + i as string) wirecolor: [random 0 255, random 0 255, 0]
    )
  )
)


-- G
-- Hacer el código que compruebe si en la escena hay objetos
-- Si en la escena hay 0 objetos debe crear 2 cajas rojas y 3 points Helpers verdes
-- Si en la escena hay de 4 a 10 objetos debe crear 3 Círculos amarillos con 10 unidades de separación en el eje Z.
-- Si en la escena hay más de 10 objetos debe duplicarse la escala a todos.

Objetosx = #()
sepz = 10
ob = random 1 10

for i =1 to ob do (
  box name: ("caja" + i as string)
)

Objetosx = for i in objects collect i
if Objetosx.count > 0 then (
  print "Si hay objetos"
) else if Objetosx.count >= 4 and Objetosx.count <= 10 then (
  for i=1 to 3 do (
    sphere name: ("S" + i as string) wirecolor: [255, 255 , 0] pos:[0, 0 , sepz * 1]
  )
) else if Objetosx.count >= 10 then (
  for i=1 to Objetosx.count do (
    Objetosx[i].scale *= 2
  )
)

-- H
-- Debe clasificar los objetos por color rojo y revisar si su escala es incorrecta
-- guardará la información del nombre del objeto, su wirecolor y si su escala es incorrecta o no lo es, usando la palabra "Correcta" e "Incorrecta"
-- Debe clasificar los objetos por color verde y revisar si su escala es correcta
-- guardará la información del nombre del objeto, su wirecolor y si su escala es incorrecta o no lo es, usando la palabra "Correcta" e "Incorrecta"
-- Todos los objetos Verdes o Rojos con la escala incorrecta deberán cambiar su nombre cada uno a un nombre único con base en la palabra "Incorrecta" guardada +
--el nombre original del objeto y su wirecolor deberá de cambiar a amarillo.
-- Todos los objetos verdes o Rojos con la escala correcta deberán cambiar su nombre cada uno a un nombre único con base en la palabra "Correcta" guardada +
--el nombre original del objeto y su wirecolor deberá de cambiar a azul.
-- Se debe imprimir en formato de columnas el #Objetos Totales, NombreObjeto orig, wirecolor Orig, Escala, Nombre Cambiado, wirecolor final
-- y en cada renglón poner el número de objetos totales, Nombre original del objeto, valor del color, si es correcta o incorrecta, Su nombre cambiado, y su valor de color en Texto "Amarillo" o "Azul"

obr =#()
rojozt = [255,0,0]
scala = [1,1,1]
correcta = #()
incorrecta = #()
obr = for o in objects where o.wirecolor == rojozt collect o
for i=1 to obr.count do (
   if obr[i].scale == scala then (
     print "Correcta"
     obr[i].wirecolor = [0,0,255]
     obr[i].name = "Correcto" + obr[i].baseobject as string
     append correcta obr[i]
   ) else (
     print "Incorrecta"
     obr[i].wirecolor = [255,255,0]
     obr[i].name = "Incorrecto" + obr[i].baseobject as string
     append incorrecta obr[i]
   )
)

obg =#()
verdex = [0,255,0]
obg = for o in objects where o.wirecolor == verdex collect o
for i=1 to obg.count do (
   if obg[i].scale == scala then (
     print "Correcta"
     obg[i].wirecolor = [0,0,255]
     obg[i].name = "Correcto" + obg[i].baseobject as string
     append correcta obg[i]
   ) else (
     print "Incorrecta"
     obg[i].wirecolor = [255,255,0]
     obg[i].name = "Incorrecto" + obg[i].baseobject as string
     append incorrecta obg[i]
   )
)

closelog() --cierra archivo