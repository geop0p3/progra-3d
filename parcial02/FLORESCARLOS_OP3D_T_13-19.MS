openlog "C:\Users\pope_\Documents\Tridimin\FLORESCARLOS_OP3D_T_13-19.txt" mode: "a" --Abre un log y lo crea

-- Carlos Ignacio Flores Soto
-- Actividades 13 a 19 / Mayo 2 del 2018

-- ACTIVIDAD 13
--  CREAR UNA TETERA DE RADIO 5 COLOR CIAN EN ESCENA EN LA POSICIÓN [0,-70,0] CON EL NOMBRE T1 CAMBIAR EL TAMAÑO DE LA
-- ESFERA E1 A 4 UNIDADES EN SU RADIO HACER LA TETERA 2 VECES MÁS GRANDE MOSTRAR EN UN FORMAT EL NOMBRE DE CADA UNA DE LAS GEOMETRÍAS EN LA ESCENA Y SU VALOR DE  ESCALA EN UN SOLO FORMAT

Esfera = sphere name: "E1" wirecolor:GREEN pos:[0,-45, 25]
Tetera = teapot name: "T1" radius: 5 pos:[0, -70, 0]
Esfera.radius = 4
Tetera.radius *= 2
arreglo = for i in objects collect i
for i = 1 to arreglo.count do
format "Nombre Objeto:% Escala:%\n" arreglo[i].name arreglo[i].scale

-- ACTIVIDAD 14
-- ES CORREGIR LA ESCALA DE LA TETERA, DESPUÉS AUMENTAR SU RADIO A 15 UNIDADES DESACTIVAR PROPIEDADES DE LOS OBJETOS DE DISPLAY
Tetera.radius = 15
resetscale tetera
for i = 1 to arreglo.count do
hide arreglo[i]

-- ACTIVIDAD 15
-- CREAR UN PLANO CON EL NOMBRE DE P1 COLOR AMARILLO EN LA POSICIÓN [0,0,80]

Plano = plane name: "P1" wirecolor: YELLOW pos:[0,0,80]

-- ACTIVIDAD 16
-- CREAR 6 CAJAS DE 5 UNIDADES EN SUS DIMENSIONES CADA UNA, CON SEPARACIÓN ENTRE SÍ DE 20
-- UNIDADES EN X, CADA UNA DE ELLAS SE LES ASIGNARÁ UN COLOR  DESDE ROJO, VERDE, AZUL, CIAN,
-- MAGENTA Y AMARILLO EN ORDEN, SUS NOMBRES SERÁN C1 A C6. LUEGO DE CREARLAS
x = 20
colox=#(RED, GREEN, BLUE, color 0 255 255, color 255 0 255, color 255 255 0 )
arreglo2 = #()
for i=1 to 6 do (
caja = box name:("C" + i as string) pos:[x * i, 0, 0] wirecolor: colox[i] width:5 height: 5 length:5
append arreglo2 caja)



--ACTIVIDAD 17
--LA PRIMERA CAJA DEBE ESTAR FREEZEADA LA SEGUNDA CAJA DEBE ESTAR ACTIVO EL BACKFACECULL 
-- LA TERCERA CAJA DEBE MOVERSE A 20 UNIDADES MÁS EN EL EJE Z LA CUARTA DEBE ESCALARSE 3 VECES MÁS 
-- LA QUINTA DEBE ROTAR 45 GRADOS EN Z LA SEXTA DEBE CAMBIAR SUS DIMENSIONES A 40 EN CADA PROPIEDAD MOVERSE A 200 UNIDADES EN X Y RESETEARLE LA ESCALA.
freeze arreglo2[1]
arreglo2[2].backfacecull=true
arreglo2[3].pos.z += 20
arreglo2[4].scale *= 3
rot_box = eulerangles 0 0 45
rotate arreglo2[5] rot_box
arreglo2[6].height = 40; arreglo2[6].height = 40; arreglo2[6].length = 40;
move arreglo2[6] [200,0,0]
resetscale arreglo2[6]

--ACTIVIDAD 18
-- CREAR UN ARREGLO C1 VACÍO 
--CREAR UN ARREGLO N1 VACÍO 
--AGREGAR AL ARREGLO C1 LOS VALORES DE COLOR DE CADA UNA DE LAS CAJAS EN LA ESCENA DE UNO POR UNO  
--AGREGAR AL ARREGLO N1 EL NOMBRE DE CADA UNA DE LAS CAJAS EN LA ESCENA DE UNO POR UNO DE LA CAJA 6 A LA 1

C1 = #()
N1 = #()
colur= null
nombre = null
for i=1 to arreglo2.count do(
	append C1 arreglo2[i].wirecolor
	append N1 arreglo2[i].name
)
N1[5]
--ACTIVIDAD 19 
--AGREGAR EN EL ARREGLO N1 DESPUÉS DEL NOMBRE DE CADA UNO DE LAS CAJAS EL COLOR QUE LE CORRESPONDE, POR EJEMPLO ("C1", (COLOR 255 0 0) ... ETC) ESTO UTILIZANDO EL ARREGLO DE C1
for i=1 to N1.count do (
g = i +1 
	insertitem C1[i] N1 g
)
N1[1]
	
closelog() --cierra archivo