/*
Esqueleto de Interface
Configuración de Escena
Carlos Flores
Examen 4to Parcial 26 de Julio 2018
*/

rollout ConfigurarEscena "Configurar Escena" width:249 height:411
(
	GroupBox 'grp1' "Configurar Escena" pos:[3,8] width:237 height:365 enabled:true align:#left
	label 'lbl1' "Nombre De Artista" pos:[8,40] width:99 height:17 align:#left
	label 'lbl3' "Imagen de Referencia" pos:[11,177] width:109 height:18 align:#left
	bitmap 'bmpImagen' "Bitmap" pos:[21,202] width:203 height:143 enabled:true fileName:"" align:#left
	edittext 'edtNombreArtista' "" pos:[104,36] width:115 height:17 align:#left
	button 'btnActualizar' "Actualizar" pos:[4,128] width:92 height:22 align:#left
	button 'btnConfigurar' "Configurar Escena" pos:[108,128] width:110 height:22 align:#left
	button 'btnCrear' "Crear" pos:[42,379] width:147 height:24 align:#left
	button 'btnImagen' "Elegir Imagen" pos:[130,171] width:90 height:20 align:#left
	dropdownList 'ddlTrabajo' "" pos:[106,78] width:113 height:21 items:#("Prop", "Personaje") align:#left
	label 'lbl56' "Tipo de Objeto" pos:[18,79] width:81 height:15 align:#left
)

createDialog ConfigurarEscena