/*
Script de funciones para
Configuración de Escena
Carlos Flores
Examen 4to Parcial 26 de Julio 2018
*/

--Se abre el log
openlog "C:\Users\pope_\Documents\Tridimin\funcionesE4.txt" mode: "a"

global p = undefined

--Remplazar espacios en un string por _
fn QuitarEspacios str = (
	try(
		-- Guarda la posicion de espacios en el string
		temp = findstring str " "

		-- Si hay espacios los remplaza con un "_"
		if temp != undefined then(
			nuevo_str = replace str temp 1 "_"
			return nuevo_str
		)
		else(
			-- Si no hay espacios regresa el string original
			return str
		)
	)
	catch(
		messageBox (getCurrentException())
	)
)

--Validacion que existan objetos en la escena
fn ValidarObjetos = (
	try(
		obj = for i in objects collect i

		if obj.count == 0 then(
			messageBox("No existen objetos en la escena")
			return false
		)

		return true
	)
	catch(
		messageBox (getCurrentException())
	)
)

--Cambiar escena a unidades metricas y background
fn CambiarUnidades = (
	try(
		units.DisplayType = #Metric
		units.MetricType = #Centimeters
		units.SystemType = #Centimeters
		backgroundColor = [9, 9, 9]
		messageBox ("Display: Metros, System: Centimetros, Background [9,9,9]")
	)
	catch(
		messageBox (getCurrentException())
	)
)

--Cargar la imagen a un bitmap control
fn CargarImagen = (
	try(
		--Abre el buscador para seleccionar imagen
		tex = selectBitMap()
		return tex
	)
	catch(
		messageBox (getCurrentException())
	)
)

--Crear un plano en base a una imagen
fn CrearPlano = (
	try(
		-- Guarda la informacion de la imagen seleccionada
		img = getBitMapInfo tex

		--Crea un plano con el tamaño de la imagen
			obj = for i in objects where classof i == plane collect i
			if obj.count <= 0 then (
					p = plane name:"ImagenReferencia" pos:[0,0,img[4] * 0.5] width: img[3] length: img[4]
			) else (
				delete obj
				p = plane name:"ImagenReferencia" pos:[0,0,img[4] * 0.5] width: img[3] length: img[4]
			)


		-- Asigna un material con la imagen seleccionada como textura
		meditMaterials[1].diffuseMap = Bitmaptexture fileName:img[1]
		p.material = meditMaterials[1]
		p.material.showInViewport = on

		-- Congenla el plano y lo rota 90 grados para estar en una posicion levantada
		p.showFrozenInGray = false
		p.isFrozen = true
		rotate p (eulerAngles 0 90 0)
		rotate p (eulerAngles 90 0 0)

		return p
	)
	catch(
		messageBox (getCurrentException())
	)
)

--se cierra el log
closelog()
