/*
Script de Interface
Configuración de Escena
Carlos Flores
Examen 4to Parcial 26 de Julio 2018

Por Cambiar
Si es personaje hacer que el grid este arriba
*/
GC()
include "Escena.ms"

--Se abre el log
openlog "C:\Users\pope_\Documents\Tridimin\E4.txt" mode: "a"

persistent global artista = (QuitarEspacios "Carlos Flores")

global tex = undefined --Guarda la ruta de la imagen seleccionada en el buscador
global img = undefined --Guarda la informacion de la imagen
global tipo = undefined --Guarda el tipo de plano

rollout ConfigurarEscena "Configurar Escena" width:249 height:411
(
	GroupBox 'grp1' "Configurar Escena" pos:[3,8] width:237 height:365 enabled:true align:#left
	label 'lbl1' "Nombre De Artista" pos:[8,40] width:99 height:17 align:#left
	label 'lbl3' "Imagen de Referencia" pos:[11,177] width:109 height:18 align:#left
	bitmap 'bmpImagen' "Bitmap" pos:[21,202] width:203 height:143 enabled:true fileName:"" align:#left
	edittext 'edtNombreArtista' "" pos:[104,36] width:115 height:17 align:#left
	button 'btnActualizar' "Actualizar" pos:[4,128] width:92 height:22 align:#left
	button 'btnConfigurar' "Configurar Escena" pos:[108,128] width:110 height:22 align:#left
	button 'btnCrear' "Crear" pos:[42,379] width:147 height:24 align:#left
	button 'btnImagen' "Elegir Imagen" pos:[130,171] width:90 height:20 align:#left
	dropdownList 'ddlTrabajo' "" pos:[106,78] width:113 height:21 items:#("Prop", "Personaje") align:#left
	label 'lbl56' "Tipo de Objeto" pos:[18,79] width:81 height:15 align:#left
	on ConfigurarEscena open do
	(
		edtNombreArtista.text = artista
	)
	on btnActualizar pressed do
	(
		-- Guarda los datos, si es prop o personaje y nombre de artista
		try(
			artista = (QuitarEspacios edtNombreArtista.text)
			tipo = ddlTrabajo.selected

			edtNombreArtista.text = artista

			if tipo == "Prop" then (
				messageBox ("Artista actualizado, Tipo: Prop")
			)
			else(
				messageBox ("Artista actualizado, Tipo: Personaje")
			)
		)
		catch(
			messageBox (getCurrentException())
		)
	)
	on btnConfigurar pressed do
	(
		--Cambia la configuracion de unidades y background
		CambiarUnidades()
	)
	on btnCrear pressed do
	(
		try(
			-- Se guarda el plano en una variable para utilizarlo en otras secciones
			plano = CrearPlano()
		)
		catch(
			messageBox (getCurrentException())
		)
	)
	on btnImagen pressed do
	(
		try(
			tex = CargarImagen()

			--Asigna la imagen al preview display en la interfaz
			if tex != undefined then(
				tmp = bitmap 203 143 color:black
				copy tex tmp 
				bmpImagen.bitmap = tmp
			)
		)
		catch(
			messageBox (getCurrentException())
		)
	)
)

createDialog ConfigurarEscena

--se cierra el log
closelog()
