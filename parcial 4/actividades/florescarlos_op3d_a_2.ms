fn RevTObj = (
  clearselection()
  select (for obj in geometry where iskindof obj geometryclass collect obj)
  for i = 1 to selection.count do (
    if max_t < CalculateV selection[i] do (
      max_t CalculateV selection[i]
      objG = selection[i].name
      ObjM = selection[i]
    )
    clearselection()
    return objG
  )
)

fn centerPiv obj = (
  Obj = #()
  Info = #(Clase = #(), Nombre = #(), Ocolorp = #())
  for j in objects do (
    append Obj j
    point pos:[j.center.x, j.center.y, j.center.z] wirecolor: (random red blue) name: (uniquename "H_")
  )
  format "Clase\tNombre\tColor Padre\n"
  for i = 1 to Obj.count do (
    Obj[i].parent = execute ("$P_00" + (i as string))
    execute ("$P_00" + (i as string) + ".pos=[0,0,0]")
    append Info[1] (classof Obj[i] as string)
    append Info[2] Obj[i].name
    append Info[3 Obj[i].parent.wirecolor
    format "%\t\t%\t%\n" Info [1][i] Info[2][i] Info[3][i]
  )
)
