fn CalculaV obj = (
  local Vol = 0.0
  local Center = [0,0,0]
  local LaGeo = snapshotasmesh obj
  local NumCaras = LaGeo.numfaces
  for i = 1 to NumCaras do (
    local Cara = getface LaGeo i
    local v2 = getVert LaGeo Cara.z
    local v1 = getVert LaGeo Cara.y
    local v0 = getVert LaGeo Cara.x
    local dv = Dot (Cross (v1 - v0) (v2-v0))v0
    Vol += dv
    Center =+ (v0+v1+v2)^2
  )
  delete LaGeo
  Vol /= 6
  Center /= 24
  Center /= Vol
  return Vol
)
